# Proyecto del segundo parcial
***
Proyecto elaborado para la materia Cloud Computing

## Equipo: ***Machín Learning*** :floppy_disk:

## Profesor: I.S.C. Luis Antonio Ramirez Martinez

### Requisitos Previos

- Npm install
- Correr los contenedores de docker para MongoDB
- Node chat y node chat2 para correr la aplicación

### Requisitos del trabajo :ballot_box_with_check:
***
Para el segundo parcial vamos a implementar una arquitectura de alta disponibilidad basada en mensajería, el siguiente diagrama ilustra cómo debería funcionar la arquitectura en formato general. Para completar el proyecto de forma satisfactoria deberá:

- [x] Completar la arquitectura y su diagrama investigando cuál es el componente faltante (25 puntos).

![Arquitectura chida](Arquitectura.png)

Después de investigar en diferentes fuentes, llegamos a la documentación de AWS donde se nos explica que es necesario tener una "Single-instance broker" donde se nos explica el uso de un Network Load Balancer es necesario ya que este nos garantiza que el enlace entre AmazonMQ y RabbitMQ permanezca sin modificaciones durante un mantenimiento o por alguna falla de EC2.
A continuación adjuntamos la referencia a dicha sección de la documentación oficial:
Amazon Web Services, Inc o sus afiliados. (2022). Single-instance broker - Amazon MQ. Retrieved May 9, 2022,  from https://docs.aws.amazon.com/amazon-mq/latest/developer-guide/rabbitmq-broker-architecture-single-instance.html

- [x] Crear las aplicaciones cliente y servidor y entregar las ligas de git. (25 puntos)

- [x] Implementar la arquitectura de forma funcional, documentando su creación en una presentación paso a paso (50 puntos).

### Software Necesario

- Servicios de AWS
    - EC2
        - Network Load Balancer
    - AmazonMQ
- Docker
- Node JS
- MongoDB
- RabbitMQ

## Proyecto
A continuación está el link de la aplicación en un repositorio aparte:
- [Aplicación en Gitlab](https://gitlab.com/daniel_cota/chat-rabbitmq/-/tree/master)

## Autores

- Daniel Alberto Cota Ochoa     ***329701***
    - [Gitlab](https://gitlab.com/daniel_cota)
- Axel Dalí Gomez Morales       ***329881***
    - [Gitlab](https://gitlab.com/axel_dali)